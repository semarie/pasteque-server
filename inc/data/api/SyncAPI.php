<?php
//    POS-Tech API
//
//    Copyright (C) 2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

namespace Pasteque;

class SyncAPI extends APIService {

    protected function check() {
        switch ($this->action) {
        case 'sync':
            return $this->isParamSet('cashregister');
            //case 'refresh':
        }
    }

    protected function proceed() {
        switch ($this->action) {
        case 'sync':
            // Get cash data
            $cashRegSrv = new CashRegistersService();
            $cashRegister = $cashRegSrv->search(
                    array(array("label", "=", $this->params['cashregister'])));
            if (count($cashRegister) == 0) {
                $this->fail("No cash register found");
                return;
            } else {
                $cashRegister = $cashRegister[0];
            }
            $result = array('cashregister' => $cashRegister);
            // Include running session
            $cashSrv = new CashesService();
            $cash = $cashSrv->getCashRegister($cashRegister->id);
            if ($cash === null || $cash->isClosed()) {
                $cash = null;
            }
            $result['cash'] = $cash;
            // paymentmodes
            $pmSrv = new PaymentModesService();
            $pm = $pmSrv->getAll();
            for ($i = 0; $i < count($pm); $i++) {
                if ($pm[$i]->hasImage) {
                    $image = $pmSrv->getImage($pm[$i]->id);
                    $pm[$i]->image = base64_encode($image);
                } else {
                    $pm[$i]->image = null;
                }
            }
            $result['paymentmodes'] = $pm;
            // Include location and stock
            $locationSrv = new LocationsService();
            $location = $locationSrv->get($cashRegister->locationId);
            $stockSrv = new StocksService();
            $stock = $stockSrv->getLevels($cashRegister->locationId);
            $result['location'] = $location;
            $result['stock'] = $stock;
            // Get currencies
            $currSrv = new CurrenciesService();
            $currencies = $currSrv->getAll();
            $result['currencies'] = $currencies;
            // Include places for restaurant mode
            $placesSrv = new PlacesService();
            $floors = $placesSrv->getAllFloors();
            $result['floors'] = $floors;
            // Get users
            $usersSrv = new UsersService();
            $users = $usersSrv->getAll();
            $rolesSrv = new RolesService();
            $roles = $rolesSrv->getAll();
            $result['users'] = $users;
            $result['roles'] = $roles;
            // Get catalog
            // taxes
            $taxSrv = new TaxesService();
            $tax = $taxSrv->getAll();
            $catSrv = new CategoriesService();
            $categories = $catSrv->getAll();
            for ($i = 0; $i < count($categories); $i++) {
                if ($categories[$i]->hasImage) {
                    $image = $catSrv->getImage($categories[$i]->id);
                    $categories[$i]->image = base64_encode($image);
                } else {
                    $categories[$i]->image = null;
                }
            }
            $prdSrv = new ProductsService();
            $products = $prdSrv->getAll();
            for ($i = 0; $i < count($products); $i++) {
                if ($products[$i]->hasImage) {
                    $image = $prdSrv->getImage($products[$i]->id);
                    $products[$i]->image = base64_encode($image);
                } else {
                    $products[$i]->image = null;
                }
            }
            $cmpSrv = new CompositionsService();
            $compositions = $cmpSrv->getAll();
            $taSrv = new TariffAreasService();
            $ta = $taSrv->getAll();
            $discSrv = new DiscountsService();
            $discounts = $discSrv->getAll();
            $result['taxes'] = $tax;
            $result['categories'] = $categories;
            $result['products'] = $products;
            $result['compositions'] = $compositions;
            $result['tariffareas'] = $ta;
            $result['discounts'] = $discounts;
            // Get customers
            $custSrv = new CustomersService();
            $customers = $custSrv->getAll();
            $topCustomers = $custSrv->getTop();
            $discprofSrv = new DiscountProfilesService();
            $discountProfiles = $discprofSrv->getAll();
            $result['customers'] = $customers;
            $result['topcustomers'] = $topCustomers;
            $result['discountprofiles'] = $discountProfiles;
            // Get resources
            $resSrv = new ResourcesService();
            $res = $resSrv->getAll();
            for ($i = 0; $i < count($res); $i++) {
                if ($res[$i]->type != Resource::TYPE_TEXT) {
                    $res[$i]->content = base64_encode($res[$i]->content);
                }
            }
            $result['resources'] = $res;
            // Send back everything
            $this->succeed($result);
        }
    }
}
